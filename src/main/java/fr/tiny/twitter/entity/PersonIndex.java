package fr.tiny.twitter.entity;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(identityType=IdentityType.APPLICATION)
public class PersonIndex {

	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private List<Key> following;
	
	@Persistent
	private List<Key> followers;
	

	public PersonIndex() {
		super();
		this.following = new ArrayList<Key>();
		this.followers = new ArrayList<Key>();
	}

	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}

	public List<Key> getFollowing() {
		return following;
	}

	public void setFollowing(List<Key> following) {
		this.following = following;
	}

	public List<Key> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Key> followers) {
		this.followers = followers;
	}
	
	
	public void addFollower(Person person) {
		this.followers.add(person.getId());
	}
	
	public void addFollowing(Person person) {
		this.following.add(person.getId());
	}

	
	
}
