package fr.tiny.twitter.entity;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable
public class PostIndex {

	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private List<Key> followers;
	
	public PostIndex() {
		super();
	}

	
	public PostIndex(Post parent) {
		super();
		this.id = KeyFactory.createKey(parent.getIdPost(),PostIndex.class.getSimpleName(), "postIndexKey"+parent.getCreationDate().hashCode()+parent.getSender().getUsername());
		this.followers = new ArrayList<Key>();
	}


	public Key getId() {
		return id;
	}


	public void setId(Key id) {
		this.id = id;
	}


	public List<Key> getFollowers() {
		return followers;
	}


	public void setFollowers(List<Key> followers) {
		this.followers = followers;
	}
	
	public void addFollower(Person person) {
		this.followers.add(person.getId());
	}

}
