package fr.tiny.twitter.entity;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;


@PersistenceCapable(identityType=IdentityType.APPLICATION)
public class Person {
	
	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private int nbFollowers=0;
	
	@Persistent
	private String email;
	
	@Persistent
	private Date inscriptionDate;
	
	@Persistent
	private String username;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String user) {
		username=user;	
	}
	public String getEmail() {
		return email;
	}

	public void incrementNbFollowers() {
		this.nbFollowers++;
	}
	
	public int getNbFollowers() {
		return this.nbFollowers;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}



	


	public Date getInscriptionDate() {
		return inscriptionDate;
	}



	public void setInscriptionDate(Date inscriptionDate) {
		this.inscriptionDate = inscriptionDate;
	}



	public void setId(Key key) {
        this.id = key;
    }
	
	
	
	public Key getId() {
		return id;
		
	}



	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	

}
