package fr.tiny.twitter.entity;

import java.util.Date;

import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Column;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class Post {
	
	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key idPost;
	
	@Embedded(members = {
        @Persistent(name="username", columns=@Column(name="username")),
        @Persistent(name="inscriptionDate", columns=@Column(name="inscriptionDate")),
        @Persistent(name="email", columns=@Column(name="email")),
    })
	private Person sender;

	@Persistent
	private Text body;
	
	@Persistent
	private Date creationDate;

	
	public Key getIdPost() {
		return idPost;
	}

	public void setIdPost(Key idPost) {
		this.idPost = idPost;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Person getSender() {
		return sender;
	}

	public void setSender(Person sender) {
		this.sender = sender;
	}

	public Text getBody() {
		return body;
	}

	public void setBody(Text body) {
		this.body = body;
	
	}
	
	

}
