package fr.tiny.twitter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import fr.tiny.twitter.entity.Person;
import fr.tiny.twitter.entity.PersonIndex;
import fr.tiny.twitter.entity.Post;
import fr.tiny.twitter.entity.PostIndex;
import fr.tiny.twitter.form.FormBuilder;
import fr.tiny.twitter.form.PostType;

@Api(
		name = "post",
		version = "v1"
 )
public class PostService {

	@ApiMethod(
			name="AllMessages",
			path="posts",
			httpMethod="GET"
	)
	public CollectionResponse<Post> indexAction(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit){
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Post> execute = null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Post.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<Post>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Post obj : execute)
				;
		} finally {
			//mgr.close();
		}
		
		return CollectionResponse.<Post>builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	
	
	
	
	
	@ApiMethod(
			name="Tweet",
			path="users/{username}/posts",
			httpMethod="POST"
	)
	public Post postAction(@Named("username") String idUser, PostType postType) throws NotFoundException {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Person person = this.getPerson(idUser, pm);
		if (person == null) {
			throw new NotFoundException(idUser+" not found");
		}
		Post post = FormBuilder.getPost(person, postType);
		PostIndex postIndex = new PostIndex(post);
		Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
		PersonIndex  pi = pm.getObjectById(PersonIndex.class, personIndexKey);
		postIndex.setFollowers(pi.getFollowers());
		try {
	        pm.makePersistent(post);
	        pm.makePersistent(postIndex);
	    } finally {
	    		pm.close();
	        
	    }
		return post;
	}
	
	private Person getPerson(String username, PersistenceManager pm) {
		Key idExpected = KeyFactory.createKey(Person.class.getSimpleName(), username);
		Query q2 = pm.newQuery(Person.class);
		q2.setFilter("id == idExpected");
		q2.declareParameters("String idExpected");
		List<Person> l = (List<Person>) q2.execute(username);
		if(l.size()>0) {
			return l.get(0);
		}
		return null;
	}
	
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
