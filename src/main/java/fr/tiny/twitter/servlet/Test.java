package fr.tiny.twitter.servlet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.javafaker.Faker;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

import fr.tiny.twitter.PMF;
import fr.tiny.twitter.entity.Person;
import fr.tiny.twitter.entity.PersonIndex;
import fr.tiny.twitter.entity.Post;
import fr.tiny.twitter.entity.PostIndex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.ResponseCache;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "test", urlPatterns = {"/test"})
public class Test extends HttpServlet{

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    		boolean createPerson = Boolean.parseBoolean(request.getParameter("createPerson")) ;
    		boolean createPersonIndex = Boolean.parseBoolean(request.getParameter("createPersonIndex")) ;
    		boolean createPost = Boolean.parseBoolean(request.getParameter("createPost")) ;
    		boolean follow = Boolean.parseBoolean(request.getParameter("follow")) ;
    		
    		int size = 2000;
	    	if(request.getParameter("size")!=null) {
	    		
	    		size = Integer.parseInt(request.getParameter("size")) ;
	    	}
	    	
	    	int start = 0;
	    	if(request.getParameter("start")!=null) {
	    		
	    		start = Integer.parseInt(request.getParameter("start")) ;
	    	}
    		
    		if(createPerson) {
    			generatePerson(request, response, start, size);
    		}
    		
    		/*if(createPersonIndex) {
    			generatePersonIndex(request, response);
    		}*/
    		
    		if(follow) {
    			
    			generateFollow(request, response, start, size);
    		}
    		
    		if(createPost) {
    			generatePost(request, response);
    		}
    		
    		
    }
    
    
    public void generatePost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		List<Post> post = this.getPost();
		List<PostIndex> postIndex = this.getPostIndex();
		
		try {
			pm.makePersistentAll(post);
			pm.makePersistentAll(postIndex);
		} finally {
	    		pm.close();
        
		}
		
    }
    
    @SuppressWarnings("deprecation")
	private List<Post> getPost(){
		List<Post> listPost = new ArrayList<Post>();
		List<Person> people = this.getPeople();
		Faker fake = new Faker();
		if(people.size()>101) {
			for(int i=0; i<101; i++) {
				Date d = people.get(i).getInscriptionDate();
				d.setDate(d.getDate()+i);
				Key postKey = KeyFactory.createKey(Post.class.getSimpleName(),  d.hashCode()+people.get(i).getUsername());
				Post post = new Post();
				post.setSender(people.get(i));
				post.setCreationDate(d);
				post.setIdPost(postKey);
				post.setBody(new Text((fake.yoda().quote())));
				listPost.add(post);
			}
		}
		return listPost;
    }
    
    private List<PostIndex> getPostIndex(){
		List<PostIndex> listPostIndex = new ArrayList<PostIndex>();
		List<Post> posts = this.getPost();
		List<Person> people = this.getPeople();
		for(Post post : posts) {
			PostIndex postIndex = new PostIndex(post);
			postIndex.getFollowers().add(people.get(5001).getId());
			postIndex.getFollowers().add(people.get(5002).getId());
			postIndex.getFollowers().add(people.get(5003).getId());
			listPostIndex.add(postIndex);
		}
		return listPostIndex;
    }
    
    
    public void generateFollow(HttpServletRequest request, HttpServletResponse response, int start, int size) throws IOException {
    		PersistenceManager pm = PMF.get().getPersistenceManager();
    		List<PersonIndex> personIndexs = this.getFollow();
    		List<PersonIndex> personIndexsG = new ArrayList<PersonIndex>();
    		
    		int tailleList = personIndexs.size();
    		
    		if(start > tailleList) {
    			start = tailleList;
    		}
    		
    		if((start+size)>personIndexs.size()) {
    			size = personIndexs.size();
    		}else {
    			size = start+size;
    		}
    		
    		for(int i=start; i<size; i++) {
    			personIndexsG.add(personIndexs.get(i));
    		}
    		
    		try {
    			pm.makePersistentAll(personIndexsG);
            
    		} finally {
    	    		pm.close();
            
    		}
    }
    
    private List<PersonIndex> getFollow(){
    		List<PersonIndex> listPIndex = this.getPeopleIndex();
    		List<Person> listP = this.getPeople();
    		int test100 = 5001;
    		int test1000 = 5002;
    		int test5000 = 5003;
    		
    		for(int i=0; i<100; i++) {
    			listPIndex.get(test100).getFollowers().add(listP.get(i).getId());
    			listPIndex.get(i).getFollowing().add(listP.get(test100).getId());
    			if(i<51) {
    				listPIndex.get(test100).getFollowing().add(listP.get(i).getId());
    				listPIndex.get(i).getFollowers().add(listP.get(test100).getId());
    			}
    		}
    		
    		for(int i=0; i<1000; i++) {
    			listPIndex.get(test1000).getFollowers().add(listP.get(i).getId());
    			listPIndex.get(i).getFollowing().add(listP.get(test1000).getId());
    			if(i<51) {
    				listPIndex.get(test1000).getFollowing().add(listP.get(i).getId());
    				listPIndex.get(i).getFollowers().add(listP.get(test1000).getId());
    			}
    		}
    		
    		for(int i=0; i<5000; i++) {
    			listPIndex.get(test5000).getFollowers().add(listP.get(i).getId());
    			listPIndex.get(i).getFollowing().add(listP.get(test5000).getId());
    			if(i<51) {
    				listPIndex.get(test5000).getFollowing().add(listP.get(i).getId());
    				listPIndex.get(i).getFollowers().add(listP.get(test5000).getId());
    			}
    		}
    		
		return listPIndex;
    }

	@SuppressWarnings("deprecation")
	public void generatePerson(HttpServletRequest request, HttpServletResponse response, int start, int size) throws IOException {
		
		List<Person> listP = this.getPeople();
		List<Person> listPG = new ArrayList<Person>();
		
		
		for(int i=0; i<100; i++) {
			listP.get(5001).incrementNbFollowers();
		}
		for(int i=0; i<1000; i++) {
			listP.get(5002).incrementNbFollowers();
		}
		for(int i=0; i<5000; i++) {
			listP.get(5003).incrementNbFollowers();
		}
		
		int tailleList = listP.size();
		
		
		
		if(start > tailleList) {
			start = tailleList;
		}
		
		if((start+size)>tailleList) {
			size = tailleList;
		}else {
			size = start+size;
		}
		
		for(int i=start; i<size; i++) {
			listPG.add(listP.get(i));
		}
		
		PersistenceManager pm = PMF.get().getPersistenceManager();

		try {
			pm.makePersistentAll(listPG);
        
		} finally {
	    		pm.close();
        
		}

	}
	
	
	private List<PersonIndex> getPeopleIndex(){

		List<PersonIndex> listPIndex = new ArrayList<PersonIndex>();
		List<Person> listP = this.getPeople();

			for(Person person : listP) {
        	   		Key personKey = KeyFactory.createKey(Person.class.getSimpleName(), person.getUsername());
        	   		Key personIndexKey = KeyFactory.createKey(personKey,PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
	    	    		PersonIndex pIndex = new PersonIndex();
	    	    		pIndex.setId(personIndexKey);
	    	    		listPIndex.add(pIndex);
			}
			
		return listPIndex;
	}
	
	
	private List<Person> getPeople(){
		
		JSONParser parser = new JSONParser();
		List<Person> listP = new ArrayList<Person>();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try {
			URL resource = getServletContext().getResource("/WEB-INF/bigDataSet.json");
			//Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"/src/main/resources/DATA/bigDataSet.json"));
			Object obj = null;
			obj = parser.parse(new FileReader(resource.getPath()));
			JSONArray jsonArray = (JSONArray) obj;
			Key personKey;
			

			for(Object u : jsonArray.toArray()) {
        	   		JSONObject jsonObject = (JSONObject) u;
        	   		Person p = new Person();
        	   		personKey = KeyFactory.createKey(Person.class.getSimpleName(), (String) jsonObject.get("username"));
        	   		p.setEmail((String) jsonObject.get("email"));
	    	    		p.setUsername((String) jsonObject.get("username"));
	    	    		

				p.setInscriptionDate(new Date((String) jsonObject.get("creationDate")));
	    	    		p.setId(personKey);
	    	    		listP.add(p);
			}

		}catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		return listP;
	}
	
	
	@SuppressWarnings("deprecation")
	public void generatePersonIndex(HttpServletRequest request, HttpServletResponse response) {
		
		List<PersonIndex> listPIndex = this.getPeopleIndex();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try {
            pm.makePersistentAll(listPIndex);
            
		} finally {
	    		pm.close();
        
		}

	}
	
	public static Date asDate(LocalDateTime localDateTime) {
	    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

}
