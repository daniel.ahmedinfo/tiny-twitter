package fr.tiny.twitter.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.jdo.Query;

import fr.tiny.twitter.PMF;
import fr.tiny.twitter.entity.Person;
import fr.tiny.twitter.entity.PersonIndex;
import fr.tiny.twitter.entity.Post;
import fr.tiny.twitter.entity.PostIndex;

import java.io.IOException;
import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "cleaningServlet", urlPatterns = {"/clean"})
public class CleaningServlet extends HttpServlet {

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
    		PersistenceManager pm = PMF.get().getPersistenceManager();
    		//CLEANING PART pour l'instant nettoie que Person et Post
    		// faut add PersonIndex
    		Query query = pm.newQuery(Person.class);
    		query.deletePersistentAll();
    		Query query2 = pm.newQuery(Post.class);
    		query2.deletePersistentAll();
    		Query query3 = pm.newQuery(PersonIndex.class);
    		query3.deletePersistentAll();
    		Query query4 = pm.newQuery(PostIndex.class);
    		query4.deletePersistentAll();
    		
    		
    		
    }

}
