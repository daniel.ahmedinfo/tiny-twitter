package fr.tiny.twitter;

import java.net.HttpRetryException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.api.server.spi.auth.EspAuthenticator;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiIssuer;
import com.google.api.server.spi.config.ApiIssuerAudience;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import fr.tiny.twitter.entity.Person;
import fr.tiny.twitter.entity.PersonIndex;
import fr.tiny.twitter.entity.Post;
import fr.tiny.twitter.entity.PostIndex;
import fr.tiny.twitter.form.FormBuilder;
import fr.tiny.twitter.form.PersonType;

import com.google.api.server.spi.auth.common.User;

/**
  * Add your first API methods in this class, or you may create another class. In that case, please
  * update your web.xml accordingly.
 **/
@Api(
		name = "person",
		version = "v1"
)
public class PersonService {
	
	
	@ApiMethod(
			name="AllUsers",
			path="users",
			scopes = {Constants.EMAIL_SCOPE},
					clientIds = {Constants.WEB_CLIENT_ID, 
						     com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID
			},
			httpMethod= ApiMethod.HttpMethod.GET
	)
	public CollectionResponse<Person> indexAction(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Nullable @Named("toptweeter") String top) {
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Person> execute = null;
		
		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Person.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}
			if(top != null) {
				query.setOrdering("nbFollowers DESC");
			}

			execute = (List<Person>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Person obj : execute)
				;
		} finally {
			mgr.close();
		}
		
		return CollectionResponse.<Person>builder().setItems(execute).setNextPageToken(cursorString).build();
		
	}
	
	@ApiMethod(
			name="find",
			path="users/{username}",
			httpMethod="GET"
	)
	public Person find(@Named("username") String user) throws NotFoundException {
		PersistenceManager mgr= this.getPersistenceManager();

		Person person = this.getPerson(user, mgr);
		
		if (person == null) {
			throw new NotFoundException(user+" not found");
		}
		
		
		return person;
		
		
	}
	
	
	/*@ApiMethod(
			name="userTimeline",
			path="users/{username}/posts",
			httpMethod="GET"
	)
	public CollectionResponse<Post> indexTimelineAction(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("username") String idUser) throws NotFoundException{
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Post> execute = new ArrayList<Post>();
		
		try {
			mgr = getPersistenceManager();
			
			Key k = KeyFactory.createKey(Person.class.getSimpleName(), idUser);
			Person person = mgr.getObjectById(Person.class, k);
			
			if (person == null) {
				throw new NotFoundException(idUser+" not found");
			}
			
			Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
			PersonIndex  pi = mgr.getObjectById(PersonIndex.class, personIndexKey);
			
			Query query = mgr.newQuery(Post.class);
			query.setFilter("sender.username == username");
			query.declareParameters("String username");
			query.setOrdering("creationDate DESC");
			
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}
			
			
			
			for (Key obj : pi.getFollowing()) {
				execute.addAll((List<Post>) query.execute(obj.getName()));
			}
			//execute = (List<Post>) query.execute(person.getEmail());
			
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Post obj : execute)
				;
				
		} finally {
			mgr.close();
		}
		
		return CollectionResponse.<Post>builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	*/
	
	@ApiMethod(
			name="userTimeline",
			path="users/{username}/posts",
			httpMethod="GET"
	)
	public CollectionResponse<Post> indexTimeline2Action(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("username") String idUser) throws NotFoundException{
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Post> execute = new ArrayList<Post>();
		
		try {
			mgr = getPersistenceManager();
			

			
			
			Person person = this.getPerson(idUser, mgr);
			
			if (person == null) {
				throw new NotFoundException(idUser+" not found");
			}
			
			
			
			Query query = mgr.newQuery(PostIndex.class);
			query.setFilter("followers == key");
			query.declareParameters("com.google.appengine.api.datastore.Key key");
			query.setOrdering("id DESC");
			
			
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}
			
			
			List<PostIndex> result = (List<PostIndex>) query.execute(person.getId());
			
			//execute = (List<Post>) query.execute(person.getId());
			
			for(PostIndex obj : result) {
				execute.add(mgr.getObjectById(Post.class, obj.getId().getParent()));
			}
			
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Post obj : execute)
				;
				
		} finally {
			//mgr.close();
		}
		
		return CollectionResponse.<Post>builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	
	
	@ApiMethod(
			name="following",
			path="users/{username}/following",
			httpMethod="GET"
	)
	public CollectionResponse<Person> indexFollowing(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("username") String idUser) throws NotFoundException{
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Person> execute = new ArrayList<Person>();
		
		try {
			mgr = getPersistenceManager();

			Person person = this.getPerson(idUser, mgr);
			
			if (person == null) {
				throw new NotFoundException(idUser+" not found");
			}
			Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
			PersonIndex  pi = mgr.getObjectById(PersonIndex.class, personIndexKey);
			for (Key obj : pi.getFollowing()) {
				Person p = mgr.getObjectById(Person.class, obj);
				execute.add(p);
			}
				
		} finally {
			mgr.close();
		}
		
		return CollectionResponse.<Person>builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	@ApiMethod(
			name="followers",
			path="users/{username}/followers",
			httpMethod="GET"
	)
	public CollectionResponse<Person> indexFollowers(@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, @Named("username") String idUser) throws NotFoundException{
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Person> execute = new ArrayList<Person>();
		
		try {
			mgr = getPersistenceManager();

			Person person = this.getPerson(idUser, mgr);
			
			if (person == null) {
				throw new NotFoundException(idUser+" not found");
			}
			Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
			PersonIndex  pi = mgr.getObjectById(PersonIndex.class, personIndexKey);
			for (Key obj : pi.getFollowers()) {
				Person p = mgr.getObjectById(Person.class, obj);
				execute.add(p);
			}
				
		} finally {
			mgr.close();
		}
		
		return CollectionResponse.<Person>builder().setItems(execute).setNextPageToken(cursorString).build();
	}
	
	
	@ApiMethod(
			name="Inscription",
			path="users",
			httpMethod="POST"
	)
	public Person postAction(PersonType peType) throws BadRequestException{
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Person person = FormBuilder.getPerson(peType);
		Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
		PersonIndex personIndex = new PersonIndex();
		
		personIndex.setId(personIndexKey);
        try {

        		if (containsPerson(person)) {
				throw new BadRequestException(peType.getUsername());
			}
        		if (containsPerson(personIndex)) {
    				throw new BadRequestException(peType.getUsername());
    			}
            pm.makePersistent(personIndex);
            pm.makePersistent(person);
           
        } finally {
        		pm.close();
            
        }
        
        return person;
	}
	
	
	@ApiMethod(
			name="Follow",
			path="users/{username}/followers/{userFollowed}",
			httpMethod="PUT"
	)
	public CollectionResponse<Person>  postFollowingAction(@Named("username") String username, @Named("userFollowed") String userFollowed) throws HttpRetryException, NotFoundException, BadRequestException{
		
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		
		Person person = this.getPerson(username, pm);
		
		
		Person person2 = this.getPerson(userFollowed, pm);
		
		if (person == null) {
			throw new NotFoundException(username+" not found");
		}
		
		if (person2==null) {
			throw new NotFoundException(userFollowed+" not found");
		}
		
		Key personIndexKey = KeyFactory.createKey(person.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person.getUsername());
		PersonIndex  pi = pm.getObjectById(PersonIndex.class, personIndexKey);
		
		Key personIndexKey2 = KeyFactory.createKey(person2.getId(),PersonIndex.class.getSimpleName(), "personIndexKey"+person2.getUsername());
		PersonIndex  pi2 = pm.getObjectById(PersonIndex.class, personIndexKey2);
		
		if(pi.getFollowing().contains(person2.getId())) {
			throw new BadRequestException(userFollowed+" already exist");
		}
		
		/*------------------------ Ajout ----------------------*/
		Query postQ = pm.newQuery(Post.class);
		postQ.setFilter("sender.username == username");
		postQ.declareParameters("String username");
		List<Post> execute = (List<Post>) postQ.execute(person2.getUsername());
		List<PostIndex> listPostInde = new ArrayList<PostIndex>();
		for (Post post : execute) {
			
			Key key = KeyFactory.createKey(post.getIdPost(),PostIndex.class.getSimpleName(), "postIndexKey"+post.getCreationDate().hashCode()+post.getSender().getUsername());
			PostIndex  pIndex = pm.getObjectById(PostIndex.class, key);
			pIndex.addFollower(person);
			listPostInde.add(pIndex);
			
		}
		/*----------------------------------------------*/
		
		pi.addFollowing(person2);
		
		pi2.addFollower(person);
		
		person2.incrementNbFollowers();
		try {
			pm.makePersistent(person2);
	        pm.makePersistent(pi);
	        pm.makePersistent(pi2);
	        pm.makePersistentAll(listPostInde);
	       
	    } finally {
	    		pm.close();
	        
	    }
        
        return indexFollowing(null, null, username);
	}
	
	
	private Person getPerson(String username, PersistenceManager pm) {
		Key idExpected = KeyFactory.createKey(Person.class.getSimpleName(), username);
		Query q2 = pm.newQuery(Person.class);
		q2.setFilter("id == idExpected");
		q2.declareParameters("String idExpected");
		List<Person> l = (List<Person>) q2.execute(idExpected);
		if(l.size()>0) {
			return l.get(0);
		}
		return null;
	}
	
	private boolean containsPerson(Person person) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Person.class, person.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private boolean containsPerson(PersonIndex person) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(PersonIndex.class, person.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
}
