package fr.tiny.twitter.form;

import java.util.Date;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import fr.tiny.twitter.entity.Person;
import fr.tiny.twitter.entity.Post;

public class FormBuilder {

	public static Person getPerson(PersonType peType) {
		Person person = new Person();
		person.setEmail(peType.getEmail());
		person.setUsername(peType.getUsername());
		Date date = new Date();
		Key personKey = KeyFactory.createKey(Person.class.getSimpleName(), person.getUsername());
		person.setId(personKey);
		person.setInscriptionDate(date);
		return person;
	}
	
	public static Post getPost(Person person, PostType postType) {
		Post post = new Post();
		Date date = new Date();
		Key postKey = KeyFactory.createKey(Post.class.getSimpleName(), +date.hashCode()+person.getUsername());
		post.setIdPost(postKey);
		post.setSender(person);
		post.setCreationDate(date);
		post.setBody(postType.getBody());
		return post;
	}

}
